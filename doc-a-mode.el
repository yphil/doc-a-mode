;;; doc-a-mode.el --- Auto-generate any Emacs module README.md

;; Copyright (C) 2017 Philippe Coatmeur
;; Author: Yassin Philip <xaccrocheur@gmail.com>
;; Maintainer: Yassin Philip <xaccrocheur@gmail.com>
;; Keywords: document
;; Version: 0.8.0
;; URL: https://bitbucket.org/yphil/doc-a-mode
;; License: GPLv3
;; Codeship-key: 8064fc10-6af1-0135-0ec0-6272dc94f4bc
;; Codeship-prj: 242012

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; Doc-a-mode allows the auto-documentation of any Emacs module
;; file. It reads module info from the file header, and then searches
;; for derived major modes definitions. After initializing those modes
;; so as to make their defined commands and key bindings available,
;; the command `information` is run on each of these commands.
;;
;; So all the commands available in the mode(s) are documented, even
;; those defined in the derived mode(s).
;;
;; Also, you can indicate the license type, package repository
;; (currently only MELPA snapshot & stable) and CI details to display
;; the corresponding badge(s).
;;
;; A README.md markdown file is then constructed in the module file's
;; directory, that tries to cope with different Markdown
;; implementations - specifically the naming of inline link anchors -
;; to produce a nicely formatted document ; Keep in mind that the
;; final rendering of said document depends on the Markdown to HTML
;; interpreter, and the CSS style directives of the final document
;; host.
;;
;; See [this very module
;; header](https://bitbucket.org/yphil/doc-a-mode/src/master/doc-a-mode.el)
;; for a working example, and [this
;; module](https://bitbucket.org/yphil/mail-bug) for an example
;; of a extensive module defining several derived modes, all with
;; their own commands.
;;
;; ## Features
;;
;; - Handles two markdown flavors
;;     - Nice (Github)
;;     - Strict (Bitbucket)
;; - Documentation of all the mode-defined interactive commands
;;     - Name
;;     - Signature
;;     - Doc string
;; - Table of the Key bindings with links to function documentation
;; - Badges
;;     - License
;;     - CI status
;;         - [CodeShip](https://app.codeship.com) (Bitbucket)
;;         - [Travis](http://travis-ci.org/) (Github)
;;     - [MELPA](http://melpa.org/) (both snapshot and stable)
;; - Works even if the module is not in `load-path`.

;; ## Install

;; Clone this repo in `load-path`, or simply `eval` the buffer.
;; Alternatively, you can wait forever for MELPA to review it ;)

;; ## Usage

;; Run `doc-a-mode RET module-file RET` ; The **README.md** file is
;; (re)generated in the file directory.

;;; Code:


(require 'subr-x)
(require 'json)

(setq debug-on-error t)
(setq case-fold-search t)

(defvar module-file)
(defvar melpa-archive-json-url "http://melpa.org/archive.json")
(defvar dam--codeship-url "https://app.codeship.com")

(defvar license-badges '(("MIT" . "[![License MIT](https://img.shields.io/badge/license-MIT-green.svg)](https://opensource.org/licenses/MIT)")
                         ("GPLv2" . "[![License GPLv2](https://img.shields.io/badge/license-GPL_v2-green.svg)](http://www.gnu.org/licenses/gpl-2.0.html)")
                         ("GPLv3" . "[![License GPLv3](https://img.shields.io/badge/license-GPL_v3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.html)")
                         ("BSD" . "[![License BSD](https://img.shields.io/badge/license-BSD-green.svg)](http://opensource.org/licenses/BSD-3-Clause)")
                         ("Apachev2" . "[![License Apache v2](https://img.shields.io/badge/license-Apache_v2-green.svg)](http://www.apache.org/licenses/LICENSE-2.0)")))

(defconst dam--regexp-key
  (rx
   (and line-start
        (zero-or-more space)
        word-start
        "key" (one-or-more not-newline) "binding\n")))

(defconst dam--regexp-keys
  (rx
   (and space space)))

(defconst dam--regexp-mode
  (rx
   (and line-start (in punctuation) word-start "define-derived-mode"
        space
        (submatch (one-or-more
                   (in word punctuation))))))

(defconst dam--regexp-interactive
  (rx
   bol "#####" space
   (minimal-match
    (and
     (submatch
      (one-or-more (any word "-"))) space
      (one-or-more anything)
      "`" (submatch (zero-or-more
                     (in word punctuation))) "'."))))

(defconst dam--regexp-function-signature
  (rx
   (and line-start
        (submatch
         "(" (one-or-more (in word punctuation blank)) ")"))))

(defconst dam--regexp-expression
  (rx
   "`" (minimal-match
        (and (submatch (zero-or-more
                        (in word punctuation))) "'"))))

(defconst dam--regexp-license
  (rx
   (and ";; License: " (submatch
                        word-start
                        (one-or-more word))) eol))

(defconst dam--regexp-melpa
  (rx
   (and ";; MELPA: " (submatch
                      word-start
                      (one-or-more word))) eol))

(defconst dam--regexp-melpa-stable
  (rx
   (and ";; MELPA-stable: " (submatch
                             word-start
                             (one-or-more word))) eol))

(defconst dam--regexp-url
  (rx
   (minimal-match
    (and
     ";; URL: " (submatch
                 word-start
                 (one-or-more anything) eol)))))

(defconst dam--regexp-codeship-key
  (rx
   (minimal-match
    (and
     ";; Codeship-key: "
     (submatch
      word-start (one-or-more anything) eol)))))

(defconst dam--regexp-codeship-prj
  (rx
   (minimal-match
    (and
     ";; Codeship-prj: "
     (submatch
      word-start (one-or-more anything) eol)))))

(defun find-a-mode (&optional file)
  "Find derived mode(s) in FILE."
  (with-temp-buffer
    (save-excursion
      (insert file))
    (let ((list '()))
      (while (re-search-forward dam--regexp-mode nil t nil)
        (add-to-list 'list (car (split-string (match-string 1)))))
      (reverse list))))

(defun get-remote-url-as-string (url)
  "Get URL and clean it up."
  (with-current-buffer (url-retrieve-synchronously url t)
    ;; remove http headers:
    (goto-char 0)
    (delete-region 1 (re-search-forward "\r?\n\r?\n"))
    (buffer-string)))

(defun get-remote-url-as-json (url)
  "Get json from URL."
  (json-read-from-string (get-remote-url-as-string url)))

(defun extract-headers (whole-buffer mode-file-name &optional type-of-info)
  "Get the header field values in WHOLE-BUFFER given MODE-FILE-NAME."
  (let ((module-name (file-name-sans-extension (file-name-nondirectory mode-file-name)))
        (header-baseline-p
         (string-prefix-p (format ";;; %s --- " (file-name-nondirectory mode-file-name)) whole-buffer))
        (header-commentary-p (string-match ";;; Commentary" whole-buffer))
        (header-license-p (string-match ";; License" whole-buffer))
        (header-code-p (string-match ";;; Code" whole-buffer))
        (header-url-p (string-match ";; URL" whole-buffer))
        (melpa-p (string-match ";; MELPA" whole-buffer))
        (melpa-stable-p (string-match ";; MELPA-stable" whole-buffer))
        (header-codeship-key-p (string-match ";; Codeship-key" whole-buffer))
        (header-codeship-prj-p (string-match ";; Codeship-prj" whole-buffer))
        (first-line)
        (module-url)
        (module-melpa)
        (module-melpa-stable)
        (license-name)
        (license-badge)
        (repo-parts)
        (repo-key)
        (repo-brand)
        (codeship-key)
        (codeship-prj)
        (codeship-badge)
        (melpa-badge)
        (module-commentary)
        (module-baseline)
        (commentary-buffer))

    (if (not header-baseline-p)
        (message "ERROR: Header should start with ;;; %s --- Blah, blah" (file-name-nondirectory mode-file-name)))
    (if (not header-commentary-p)
        (message "ERROR: Header should contain a \"Commentary\" part"))
    (if (not header-license-p)
        (message "WARNING: Header doesn\'t contain a \"URL\" part"))
    (if (not header-url-p)
        (message "WARNING: Header doesn\'t contain a \"License\" part"))
    (if (not header-codeship-key-p)
        (message "WARNING: Header doesn\'t contain a \"Codeship-key\" part"))
    (if (not header-codeship-prj-p)
        (message "WARNING: Header doesn\'t contain a \"Codeship-prj\" part"))
    (if (not header-code-p)
        (message "ERROR: Header should contain a \"Code\" part"))

    (when (and header-baseline-p
               header-commentary-p
               header-code-p)

      (with-temp-buffer
        (save-excursion
          (insert (car (split-string whole-buffer ";; Commentary:"))))
        (while (re-search-forward dam--regexp-url nil t nil)
          (setq module-url (match-string 1))))

      (cond
       ((eq 'repo-brand type-of-info)
        (if (string-prefix-p "https://bitbucket.org" module-url)
            (setq repo-brand "bitbucket")
          (setq repo-brand "github")))

       ((eq 'module-url type-of-info)
        (with-temp-buffer
          (save-excursion
            (insert (car (split-string whole-buffer ";; Commentary:"))))
          (while (re-search-forward dam--regexp-url nil t nil)
            (setq module-url (match-string 1)))))

       ((eq 'codeship-badge type-of-info)
        (when (and header-codeship-prj-p
                   header-codeship-key-p
                   header-url-p)

          (setq repo-parts (split-string module-url "/"))
          (setq repo-key (format "%s/%s"
                                 (nth (- (length repo-parts) 2) repo-parts)
                                 (nth (- (length repo-parts) 1) repo-parts)))

          (with-temp-buffer
            (save-excursion
              (insert (car (split-string whole-buffer ";; Commentary:"))))
            (while (re-search-forward dam--regexp-codeship-key nil t nil)
              (setq codeship-key (match-string 1)))
            (while (re-search-forward dam--regexp-codeship-prj nil t nil)
              (setq codeship-prj (match-string 1))))
          (setq codeship-badge
                (format
                 "[![Codeship Status for %s](%s/projects/%s/status?branch=master)](%s/projects/%s)"
                 repo-key
                 dam--codeship-url
                 codeship-key
                 dam--codeship-url
                 codeship-prj))))

       ((eq 'license-badge type-of-info)

        (with-temp-buffer
          (save-excursion
            (insert (car (split-string whole-buffer ";; Commentary:"))))
          (while (re-search-forward dam--regexp-license nil t nil)
            (setq license-name (match-string 1))))

        (if (and header-license-p license-name)
            (mapc (lambda (x)
                    (if (string-prefix-p (car x) license-name t)
                        (setq license-badge (cdr x))))
                  license-badges)))

       ((eq 'melpa-badge type-of-info)

        (with-temp-buffer
          (save-excursion
            (insert (car (split-string whole-buffer ";; Commentary:"))))
          (while (re-search-forward dam--regexp-melpa nil t nil)
            (setq module-melpa (match-string 1)))
          (while (re-search-forward dam--regexp-melpa-stable nil t nil)
            (setq module-melpa-stable (match-string 1))))

        (if (and module-melpa
                 (string-prefix-p "yes" module-melpa))
            (setq melpa-badge
                  (format "[![MELPA](https://melpa.org/packages/%s-badge.svg)](https://melpa.org/#/%s)" module-name module-name)))

        (if (and module-melpa-stable
                 (string-prefix-p "yes" module-melpa-stable))
            (setq melpa-badge
                  (concat melpa-badge " " (format "[![MELPA](https://stable.melpa.org/packages/%s-badge.svg)](https://stable.melpa.org/#/%s)" module-name module-name))))


        )

       ((eq 'module-commentary type-of-info)

        (with-temp-buffer
          (save-excursion
            (insert (car (split-string whole-buffer ";;; Code"))))
          (setq commentary-buffer (cadr (split-string (buffer-string) ";;; Commentary:"))))

        (with-temp-buffer
          (save-excursion
            (insert (format "%s" commentary-buffer)))
          (save-excursion
            (while (search-forward ";;" nil t)
              (replace-match "" nil t)))
          (save-excursion
            (while (search-forward "\n " nil t)
              (replace-match "\n" nil t)))
          (setq module-commentary (buffer-string))))

       ((eq 'module-baseline type-of-info)
        (with-temp-buffer
          (save-excursion (insert (car (split-string whole-buffer ";;; Code"))))
          (setq first-line (buffer-substring (line-beginning-position) (line-end-position))))

        (with-temp-buffer
          (save-excursion
            (insert first-line))
          (while (search-forward (format ";;; %s --- "
                                         (file-name-nondirectory mode-file-name)) nil t)
            (replace-match "" nil t))
          (setq module-baseline (buffer-string))))

       (t (message "What?")))

      (cond ((eq 'module-name type-of-info) module-name)
            ((eq 'repo-brand type-of-info) repo-brand)
            ((eq 'module-url type-of-info) module-url)
            ((eq 'codeship-badge type-of-info) codeship-badge)
            ((eq 'license-badge type-of-info) license-badge)
            ((eq 'melpa-badge type-of-info) melpa-badge)
            ((eq 'module-commentary type-of-info) module-commentary)
            ((eq 'module-baseline type-of-info) module-baseline)
            (t (message "What?"))))))

;;;###autoload
(defun doc-a-mode (module-file-name)
  "Auto-document a MODULE-FILE-NAME mode file."
  (interactive "fModule file to document:")

  (if (not (string-suffix-p ".el" module-file-name))
      (error "Not a regular Emacs module file"))

  (with-temp-buffer
    (insert-file-contents-literally module-file-name)
    (setq module-file (buffer-string)))

  (let* ((module-dir (file-name-directory module-file-name))
         (readme-file (concat module-dir "README.md"))
         (module-name (file-name-sans-extension (file-name-nondirectory module-file-name)))
         (module-baseline (extract-headers module-file module-file-name 'module-baseline))
         (module-commentary (extract-headers module-file module-file-name 'module-commentary))
         (module-license-badge (extract-headers module-file module-file-name 'license-badge))
         (codeship-badge (extract-headers module-file module-file-name 'codeship-badge))
         (melpa-badge (extract-headers module-file module-file-name 'melpa-badge))
         (module-repo-brand (extract-headers module-file module-file-name 'repo-brand))
         (module-url (extract-headers module-file module-file-name 'module-url))
         (modelist (find-a-mode module-file))
         (melpa-p (if (assoc module-name (get-remote-url-as-json melpa-archive-json-url))
                      t
                    nil))
         (function-doc-headline)
         (dam--mode-plural))

    (if (not (featurep (intern-soft module-name)))
        (require (intern-soft module-name) module-file-name))

    (if (file-regular-p readme-file)
        (delete-file readme-file))

    (with-temp-buffer
      (save-excursion
        (insert
         (format "# %s\n *%s*\n___\n" (capitalize module-name) module-baseline))
        (if module-license-badge
            (insert (format "%s " module-license-badge)))
        (if codeship-badge
            (insert (format "%s " codeship-badge)))
        (if melpa-badge
            (insert (format "%s " melpa-badge)))
        (insert (format "\n%s" module-commentary))

        (message "Modelist: %s" (length modelist))

        (when (> (length modelist) 0)
          (if (> (length modelist) 1)
              (setq dam--mode-plural "s")
            (setq dam--mode-plural ""))
          (insert "\## Major modes\n\n")
          (insert
           (format "%s introduces %d major mode%s, detailed below.\n\n"
                   (capitalize module-name)
                   (length modelist)
                   dam--mode-plural)))

        (mapc (lambda (x)
                (insert
                 (format "\### %s\n" x)
                 (format "%s\n" (mode-commands x 'header module-repo-brand))
                 (format "%s\n" (mode-commands x 'all module-repo-brand))
                 (format "___\n")))
              modelist))
      (while
          (re-search-forward dam--regexp-interactive nil t nil)
        (if (not (string-prefix-p (match-string 2) (file-name-nondirectory module-file-name)))
            (setq function-doc-headline
                  (format "##### %s\n> Defined in `%s`\n"
                          (match-string 1)
                          (match-string 2)))
          (setq function-doc-headline
                (format "##### %s" (match-string 1))))
        (replace-match function-doc-headline))

      (goto-char (point-min))
      (while (re-search-forward dam--regexp-expression nil t nil)
        (replace-match (format "`%s`" (match-string 1))))

      (goto-char (point-max))
      (if (= 0 (length modelist))
          (insert (format "___\n")))
      (insert
       (format "*%s made on %s at %s with [doc-a-mode](https://bitbucket.org/yphil/doc-a-mode)*"
               (file-name-nondirectory readme-file)
               (format-time-string "%F")
               (format-time-string "%H:%M:%S")))
      (write-region (buffer-string) nil readme-file t 0 nil nil)
      (message "Wrote %s" readme-file))))

(defun mode-commands (mode type-of-info repo-brand)
  "Print a MODE markdown documentation of type TYPE-OF-INFO."
  (interactive)

  (let ((header)
        (my-commands-list)
        (biglist '())
        (this-command)
        (anchor-prefix (if (string-prefix-p "bitbucket" repo-brand)
                           "#markdown-header-"
                         ""))
        (my-commands-buffer))
    (with-temp-buffer
      ;; Initialize the mode so that its keymap is loaded ; do this in a
      ;; separate temp buffer in case the mode is read-only
      (funcall (intern-soft mode)))

    (with-temp-buffer

      (insert (documentation (intern-soft mode)))

      (setq header
            (car (split-string (buffer-string) dam--regexp-key nil nil))
            my-commands-buffer (cadr (split-string (buffer-string) dam--regexp-key nil nil)))
      (erase-buffer)
      (save-excursion
        (insert (format "%s" my-commands-buffer)))
      (kill-line 2)
      (goto-char (point-max))
      (backward-paragraph)
      (delete-region (point) (point-max))
      (setq my-commands-list (split-string (buffer-string) "\n" t nil))
      (erase-buffer)
      (goto-char (point-min))

      (mapc (lambda (x)
              (setq this-command (string-trim (car (last (cdr (split-string x dam--regexp-keys))))))
              (if (not (or (string-prefix-p "Prefix " this-command)
                           (string-prefix-p "??" this-command)))
                  (add-to-list 'biglist
                               (cons (car (split-string x dam--regexp-keys))
                                     this-command))))
            my-commands-list)

      (when (> (length biglist) 0)

        (insert (format "#### %s Keys\n\nKey  | Binding \n------------- | ------------- \n" mode))
        (mapc (lambda (x)
                (insert (format "`%s` | [%s](%s%s)\n"
                                (car x)
                                (cdr x)
                                anchor-prefix
                                (cdr x))))
              (reverse biglist))
        (insert (format "\n#### %s Commands\n\n" mode)))

      (mapc (lambda (x)
              (insert
               (format "\##### %s\n\n" (describe-function
                                        (eval (read (format "(function %s)" (cdr x))))))))
            (reverse biglist))

      (goto-char (point-min))
      (while (re-search-forward dam--regexp-function-signature nil t nil)
        (replace-match (format "```elisp
%s
```" (match-string 1))))

      ;; (message "BIGBUF: %S" (buffer-string))

      (cond ((eq 'header type-of-info)
             header)
            ((eq 'all type-of-info)
             (buffer-string))
            (t
             (buffer-string))))))

(provide 'doc-a-mode)
;;; doc-a-mode.el ends here
