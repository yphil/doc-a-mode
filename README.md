# Doc-A-Mode
 *Auto-generate any Emacs module README.md*
___
[![License GPLv3](https://img.shields.io/badge/license-GPL_v3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Codeship Status for yphil/doc-a-mode](https://app.codeship.com/projects/8064fc10-6af1-0135-0ec0-6272dc94f4bc/status?branch=master)](https://app.codeship.com/projects/242012) 


Doc-a-mode allows the auto-documentation of any Emacs module
file. It reads module info from the file header, and then searches
for derived major modes definitions. After initializing those modes
so as to make their defined commands and key bindings available,
the command `information` is run on each of these commands.

So all the commands available in the mode(s) are documented, even
those defined in the derived mode(s).

Also, you can indicate the license type, package repository
(currently only MELPA snapshot & stable) and CI details to display
the corresponding badge(s).

A README.md markdown file is then constructed in the module file's
directory, that tries to cope with different Markdown
implementations - specifically the naming of inline link anchors -
to produce a nicely formatted document ; Keep in mind that the
final rendering of said document depends on the Markdown to HTML
interpreter, and the CSS style directives of the final document
host.

See [this very module
header](https://bitbucket.org/yphil/doc-a-mode/src/master/doc-a-mode.el)
for a working example, and [this
module](https://bitbucket.org/yphil/mail-bug) for an example
of a extensive module defining several derived modes, all with
their own commands.

## Features

- Handles two markdown flavors
    - Nice (Github)
    - Strict (Bitbucket)
- Documentation of all the mode-defined interactive commands
    - Name
    - Signature
    - Doc string
- Table of the Key bindings with links to function documentation
- Badges
    - License
    - CI status
        - [CodeShip](https://app.codeship.com) (Bitbucket)
        - [Travis](http://travis-ci.org/) (Github)
    - [MELPA](http://melpa.org/) (both snapshot and stable)
- Works even if the module is not in `load-path`.

## Install

Clone this repo in `load-path`, or simply `eval` the buffer.
Alternatively, you can wait forever for MELPA to review it ;)

## Usage

Run `doc-a-mode RET module-file RET` ; The **README.md** file is
(re)generated in the file directory.

___
*README.md made on 2017-11-22 at 11:46:33 with [doc-a-mode](https://bitbucket.org/yphil/doc-a-mode)*